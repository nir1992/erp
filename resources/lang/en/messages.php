<?php 

return [
    "not-found"                  =>"Not found.",
    "internal-error"             =>"Internal server error.",
    "unauthorized-access"        =>"Unathorized access",
    "unauthorized-access-role"   =>"Unathorized role access",
    "account-inactive"           =>"Restricted! You account is inactive.",
    "parameters-fail-validation" =>"Fail to pass validation",
    
    
	"api-add"        =>"Api added successfully",
	"category-add"   =>"Category added successfully",
	
	'admin-profile-update' =>"Your profile has successfully been updated!",

    "role-add"    =>"Role added successfully",
    "role-update" =>"Role updated successfully",
    "role-distroy" =>"Role removed successfully",

    "permission-add"    =>"Permission added successfully",
    "permission-update" =>"Permission updated successfully",
    "permission-distroy" =>"Permission removed successfully",

    "admin-add"         =>"Administrator added successfully",
    "admin-update"      =>"Administrator updated successfully",
    "admin-distroy"     =>"Administrator removed successfully",
    "admin-status"      =>"Administrator's account (:status)status updated successfully",
    'admin-update-fail' =>'Fail to update the record!',


    "user-add"            =>"User added successfully",
    "user-update"         =>"User updated successfully",
    "user-distroy"        =>"User removed successfully",
    "user-status"         =>"User's account status updated successfully",
    "user-verifed-status" =>"User's account set as :status.",
    'user-update-fail'    =>'Fail to update the record!',
    'user-not-found'      =>'No user found with this ID!',
    'user-profile-update' =>'Your profile has successfully been updated!',

   

    "driver-add"     =>"Driver added successfully",
    "driver-update"  =>"Driver updated successfully",
    "driver-distroy" =>"Driver removed successfully",
    
    "handyman-add"     => "Handyman added successfully",
    "handyman-update"  => "Handyman updated successfully",
    "handyman-distroy" => "Handyman removed successfully",



    "page-add"            =>"Page added successfully",
    "page-update"         =>"Page updated successfully",
    "page-distroy"        =>"Page removed successfully",
    "page-status"         =>"Page's account status updated successfully",
    "page-verifed-status" =>"Page's account status updated successfully",
    'page-update-fail'    =>'Fail to update the record!',
    'page-not-found'      =>'No page found with this ID!',



    "category-add"            =>"Category added successfully",
    "category-update"         =>"Category updated successfully",
    "category-distroy"        =>"Category removed successfully",
    "category-status"         =>"Category status updated successfully",
    "category-verifed-status" =>"Category status updated successfully",
    'category-update-fail'    =>'Fail to update the record!',
    'category-not-found'      =>'No page found with this ID!',
    'category-subcategory'      =>'This category has one or more subcategories, Please delete subcategories first befor deleting this category!',

    'contactus-message'      =>'Thank you for contacting us, We will get back to you soon!',


    "company-add"            =>"Company added successfully",
    "company-update"         =>"Company updated successfully",
    "company-distroy"        =>"Company removed successfully",
    "company-status"         =>"Company's status updated successfully",
    "company-verifed-status" =>"Company's account set as :status.",
    'company-update-fail'    =>'Fail to update the record!',
    'company-not-found'      =>'No company found with this ID!',

    
    "car-add"            =>"Car added successfully",
    "car-update"         =>"Car updated successfully",
    "car-distroy"        =>"Car removed successfully",
    "car-status"         =>"Car's status updated successfully",
    "car-verifed-status" =>"Car's account set as :status.",
    'car-update-fail'    =>'Fail to update the record!',
    'car-not-found'      =>'No car found with this ID!',




    //webservices

    "REGISTRATION_AND_VERIFICATION" =>  "Check your inbox for verification link!",
    "REGISTRATION_SUCCESS"          =>  "You will receive OTP shortly to verify the account",
    "REGISTRATION_DONE"             =>  "Registration done successfully",
    "REGISTRATION_FAIL"             =>  "Fail to register your account!",
    "ACCOUNT_VERFIED"               =>  "Your account has been verifed! Please login",
    "LOGIN_SUCCESSFULL"             =>  "Login successfully done",
    "ACCOUNT_VERFIED_FAIL"          =>  "Opps, your account is not verified!",
    "NO_EMAIL_FOUND"                =>  "This email is not available",
    "FORGOT_PASSWORD"               =>  "Password reset instructions have been sent to the email address entered during registration",
    "RESET_PASSWORD"                =>  "Password reset successfully",
    "NO_MATCH_FOUND"                =>  "Email OR Password does not match",
    "NO_DATA_FOUND"                 =>  "No Data Found",
    "UPDATE_ACCOUNT"                =>  "Account updated successfully",
    "NO_USER_FOUND"                 =>  "No User Found",
    "SOME_THING_WRONG"              =>  "Something Wrong, Please try again",
    "ORDER_PLACE_PENDING"           =>  "Order Place With Pending Status",
    "ORDER_PAYMENT_DONE"            =>  "Payment Done Successfully",
    "DRIVER_SERCHING"               =>  "Searching driver",
    "DRIVER_ACCEPT"                 =>  "Driver accepted request",
    "DRIVER_REJECT"                 =>  "Driver rejected request",
];